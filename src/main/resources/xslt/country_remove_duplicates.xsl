<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" indent="yes" />
	<xsl:template match="/">
		<NewDataSet>
			<xsl:for-each-group select="/NewDataSet/Table"
				group-by="countrycode | CountryCode">
				<xsl:sequence select="." />
			</xsl:for-each-group>
		</NewDataSet>
	</xsl:template>
</xsl:stylesheet>