package pl.pgs.camel;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

public class CountryDetailsInterface {

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCountries(){return null;}
	
	@GET
	@Path("/{code}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCountryByCountryCode(@PathParam("code") String code){return null;}
	
	@GET
	@Path("/currencies/")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCurrencies(){return null;}
	
	@GET
	@Path("/currencies/{countryName}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCurrencyByCountry(@PathParam("countryName") String countryName){return null;}
	
	@GET
	@Path("/currency-codes/")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCurrencyCode(){return null;}
	
	@GET
	@Path("/currency-codes/{currencyName}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCurrencyCodeByCurrencyName(@PathParam("currencyName") String currencyName){return null;}
	
	
}
