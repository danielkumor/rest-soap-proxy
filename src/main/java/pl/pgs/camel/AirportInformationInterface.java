package pl.pgs.camel;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

public class AirportInformationInterface {
	
	@GET
	@Path("/{code}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAirportInformationByAirportCode(@PathParam("code") String code){return null;}
	
	@GET
	@Path("/country/{code}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCountryByCountryCode(@PathParam("code") String code){return null;}
	
	@GET
	@Path("/ISOCountryCode/{code}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAirportInformationByISOCountryCode(@PathParam("code") String code){return null;}

}
